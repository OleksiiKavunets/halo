package common;

import com.jayway.restassured.RestAssured;
import common.env.EnvironmentSetUp;
import org.testng.annotations.BeforeClass;

public class BaseTest {

    protected RestService service;

    @BeforeClass
    public void setUp() {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();
        RestAssured.baseURI = EnvironmentSetUp.getInstance().getBaseUrl();

        service = new RestService();
    }
}

import common.BaseTest;
import common.utils.AuthEntity;
import org.testng.annotations.Test;

import java.io.FileNotFoundException;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsNot.not;

public class AuthenticationTest extends BaseTest {

    @Test
    public void testCanLoginWithValidCredentials() {

        service.login(AuthEntity.VALID_ENTITY).

        then().
                assertThat().
                statusCode(200).
                body("$", hasKey("access_token")).
                body("access_token", not(equalTo(nullValue()))).
                body("$", hasKey("refresh_token")).
                body("$", hasKey("expires_in")).
                body("scope", equalTo("admin")).
                body("token_type", equalTo("Bearer"));
    }

    @Test
    public void testCanLoginWithInvalidEmail(){

        service.login(AuthEntity.WITH_INVALID_EMAIL).

        then().
                assertThat().
                statusCode(401).
                body("error.message", equalTo("Incorrect email or password"));
    }

    @Test
    public void testCanLoginWithInvalidPassword(){

        service.login(AuthEntity.WITH_INVALID_PASSWORD).

        then().
                assertThat().
                statusCode(401).
                body("error.message", equalTo("Incorrect email or password"));
    }

    @Test
    public void testCanLoginWithMissingEmail() throws FileNotFoundException {

        service.login(AuthEntity.WITH_MISSING_EMAIL).

        then().
                assertThat().
                statusCode(400).
                body("error.message", equalTo("Username is mandatory"));
    }

    @Test
    public void testCanLoginWithMissingPassword(){

        service.login(AuthEntity.WITH_MISSING_PASSWORD).

        then().
                assertThat().
                statusCode(400).
                body("error.message", equalTo("Password is mandatory"));
    }

    @Test
    public void testCanLoginWithoutGrantType(){

        service.login(AuthEntity.WITH_MISSING_GRANT_TYPE).

        then().
                assertThat().
                body("error.message", equalTo("Grant type is mandatory"));
    }
}


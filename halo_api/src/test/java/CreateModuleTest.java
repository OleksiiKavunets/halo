import common.BaseTest;
import common.pojo.Module;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.annotations.Test;
import org.unitils.reflectionassert.ReflectionAssert;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class CreateModuleTest extends BaseTest {

    private String moduleName = RandomStringUtils.randomAlphabetic(10);

    Module module = new Module()
            .withName(moduleName)
            .isSingle(true);

    @Test
    public void testCanCreateModule(){

        Module expectedModule = service.createModule(module).

        then().
        assertThat().
                statusCode(200).
                body("name", equalTo(module.getter.getName())).
                body("isSingle", equalTo(module.getter.getIsSingle())).
                extract().body().as(Module.class);

        Module actualModule = service.getModule(expectedModule.getter.getId()).getBody().as(Module.class);
        ReflectionAssert.assertReflectionEquals(expectedModule, actualModule);
    }

    @Test
    public void testCanCreateModuleWithExistingName(){
        String existingModuleName = service.getExistingModuleName();
        module.withName(existingModuleName);

        service.createModule(module).

        then().
                root("error").
                assertThat().
                statusCode(400).
                body("message", containsString("You cannot create a module with the same name for the customer")).
                body("extra.name", equalTo(existingModuleName));
    }

    @Test
    public void testCanCreateModuleWithoutName(){

        module.withName(null);

        service.createModule(module).

        then().
                assertThat().
                statusCode(400).
                root("error").
                body("message", equalTo("One or more fields have validation problems")).
                body("extra.name", equalTo("mandatory_attribute"));

    }
}

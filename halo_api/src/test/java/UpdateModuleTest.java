import common.BaseTest;
import common.pojo.Module;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.equalTo;

public class UpdateModuleTest extends BaseTest {

    String moduleName = RandomStringUtils.randomAlphabetic(10);

    Module module = new Module()
            .withName(moduleName)
            .isSingle(true);

    @Test
    public void testCanUpdateModule(){
        module = service.createModule(module).getBody().as(Module.class);
        module.withName(moduleName + "1");

        service.updateModule(module).

        then().
                assertThat().
                statusCode(403).
                body("error.message", equalTo("Your role is not granted for requesting this operation"));
    }
}

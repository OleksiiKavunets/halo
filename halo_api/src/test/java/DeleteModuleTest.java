import common.BaseTest;
import common.pojo.Module;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.annotations.Test;

import static org.hamcrest.CoreMatchers.*;

public class DeleteModuleTest extends BaseTest {

    String randomString = RandomStringUtils.randomAlphabetic(10);

    Module module = new Module()
                    .withName(randomString)
                    .isSingle(true);

    @Test
    public void testCanDeleteModule(){

        module = service
                .createModule(module)
                .getBody().as(Module.class);

        service.deleteModule(module.getter.getId()).

        then().
                assertThat().
                statusCode(200).
                body("deletedAt", not(equalTo(nullValue()))).
                body("deletedBy", not(equalTo(nullValue())));

        service.getModule(module.getter.getId()).
                then().
                assertThat().
                statusCode(404).
                body("error.message", equalTo("Module not found"));
    }

    @Test
    public void testCanDeleteNotExistingModule(){

        service.deleteModule(randomString).

        then().
                root("error").
                assertThat().
                statusCode(404).
                body("message", equalTo("Module not found"));
    }
}

package common.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "isSingle",
        "name",
        "tags",
        "defaultLanguage",
        "pushTemplates",
        "customerId",
        "createdBy",
        "updatedBy",
        "externalId",
        "deletedAt",
        "deletedBy",
        "createdAt",
        "updatedAt",
        "id"
})
public class Module implements Serializable
{

    @JsonProperty("isSingle")
    private Boolean isSingle;
    @JsonProperty("name")
    private String name;
    @JsonProperty("tags")
    private List<Object> tags = null;
    @JsonProperty("defaultLanguage")
    private Object defaultLanguage;
    @JsonProperty("pushTemplates")
    private List<Object> pushTemplates = null;
    @JsonProperty("customerId")
    private Integer customerId;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("updatedBy")
    private String updatedBy;
    @JsonProperty("externalId")
    private Object externalId;
    @JsonProperty("deletedAt")
    private Object deletedAt;
    @JsonProperty("deletedBy")
    private Object deletedBy;
    @JsonProperty("createdAt")
    private Long createdAt;
    @JsonProperty("updatedAt")
    private Long updatedAt;
    @JsonProperty("id")
    private String id;
    private final static long serialVersionUID = 2051849481964146922L;

    @JsonIgnore
    public Getter getter = new Getter();

    public Module isSingle(Boolean isSingle) {
        this.isSingle = isSingle;
        return this;
    }

    public Module withName(String name) {
        this.name = name;
        return this;
    }

    public Module withTags(List<Object> tags) {
        this.tags = tags;
        return this;
    }

    public Module withDefaultLanguage(Object defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
        return this;
    }

    public Module withPushTemplates(List<Object> pushTemplates) {
        this.pushTemplates = pushTemplates;
        return this;
    }

    public Module withCustomerId(Integer customerId) {
        this.customerId = customerId;
        return this;
    }

    public Module withCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public Module withUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return this;
    }

    public Module withExternalId(Object externalId) {
        this.externalId = externalId;
        return this;
    }

    public Module withDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
        return this;
    }

    public Module withDeletedBy(Object deletedBy) {
        this.deletedBy = deletedBy;
        return this;
    }

    public Module withCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public Module withUpdatedAt(Long updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public Module withId(String id) {
        this.id = id;
        return this;
    }

    public class Getter {

        public Boolean getIsSingle() {
            return isSingle;
        }

        public String getName() {
            return name;
        }

        public List<Object> getTags() {
            return tags;
        }

        public Object getDefaultLanguage() {
            return defaultLanguage;
        }

        public List<Object> getPushTemplates() {
            return pushTemplates;
        }

        public Integer getCustomerId() {
            return customerId;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public String getUpdatedBy() {
            return updatedBy;
        }

        public Object getExternalId() {
            return externalId;
        }

        public Object getDeletedBy() {
            return deletedBy;
        }

        public String getId() {
            return id;
        }

    }

}
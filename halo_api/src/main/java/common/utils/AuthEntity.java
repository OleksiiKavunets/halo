package common.utils;

import common.env.EnvironmentSetUp;
import org.apache.commons.lang.RandomStringUtils;

public class AuthEntity {

    private static EnvironmentSetUp env = EnvironmentSetUp.getInstance();
    private static String randomString = RandomStringUtils.randomAlphabetic(10);
    private static String ivalidEmail = new StringBuilder(randomString).append("@gmail.com").toString();

    public static final String VALID_ENTITY = String.format(
            "{\"username\":\"%s\",\"password\":\"%s\",\"grant_type\":\"password\"}", env.getEmail(), env.getPassword());

    public static final String WITH_INVALID_EMAIL = String.format(
            "{\"username\":\"%s\",\"password\":\"%s\",\"grant_type\":\"password\"}", ivalidEmail, env.getPassword());

    public static final String WITH_INVALID_PASSWORD = String.format(
            "{\"username\":\"%s\",\"password\":\"%s\",\"grant_type\":\"password\"}", env.getEmail(), randomString);

    public static final String WITH_MISSING_EMAIL = String.format(
            "{\"password\":\"%s\",\"grant_type\":\"password\"}", randomString);

    public static final String WITH_MISSING_PASSWORD = String.format(
            "{\"username\":\"%s\",\"grant_type\":\"password\"}", env.getEmail());

    public static final String WITH_MISSING_GRANT_TYPE = String.format(
            "{\"username\":\"%s\",\"password\":\"%s\"}", env.getEmail(), env.getPassword());
}

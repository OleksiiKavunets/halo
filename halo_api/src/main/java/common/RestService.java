package common;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.internal.mapper.ObjectMapperType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import common.pojo.Module;
import common.utils.AuthEntity;

import static com.jayway.restassured.RestAssured.with;

public class RestService {

    public static final String OAUTH_TOKEN_PATH = "/api/oauth/token";
    public static final String CONTENT_MODULE_PATH = "/api/generalcontent/module";

    private String accessToken;

    public String getToken() {
        if(accessToken == null) authenticate();
        return accessToken;
    }

    private RequestSpecification getAuthSpec(){
        return with().
                auth().oauth2(getToken()).
                contentType(ContentType.JSON);
    }

    private RestService authenticate(){
        if(accessToken == null)
        accessToken = login(AuthEntity.VALID_ENTITY).

        then().extract().path("access_token");

        return this;
    }

    public Response login(String entity){
        return with().contentType(ContentType.JSON).body(entity).post(OAUTH_TOKEN_PATH);
    }

    public Response getModule(String id){
        return getAuthSpec().get(CONTENT_MODULE_PATH + "/" + id);
    }

    public String getExistingModuleName(){

         return getAuthSpec().
                param("page", "1").
                param("limit", "1").
                get(CONTENT_MODULE_PATH).
                then().extract().body().path("items[0].name");
    }

    public Response createModule(Module module){
        return getAuthSpec().body(module, ObjectMapperType.JACKSON_2).post(CONTENT_MODULE_PATH);
    }

    public Response updateModule(Module module){
        return getAuthSpec().body(module).post(CONTENT_MODULE_PATH + "/" + module.getter.getId());
    }

    public Response deleteModule(String moduleId){
        return getAuthSpec().delete(CONTENT_MODULE_PATH + "/" + moduleId);
    }

}

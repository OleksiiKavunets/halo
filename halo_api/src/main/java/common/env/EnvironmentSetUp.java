package common.env;


import common.utils.PropertyUtil;

public class EnvironmentSetUp {

    private static EnvironmentSetUp instance;
    private Server server;
    private PropertyUtil prop;

    private Server defaultServer = Server.STAGE;

    private EnvironmentSetUp() {
        server = System.getProperty("server") != null ? Server.valueOf(System.getProperty("server")) : defaultServer;
        prop = new PropertyUtil(String.format("/envproperties/%s_env.property", server.toString().toLowerCase()));
    }

    public static synchronized EnvironmentSetUp getInstance(){
        if(instance == null)
            instance = new EnvironmentSetUp();
        return instance;
    }

    public String getBaseUrl() {
        return prop.getProperty("base.url");
    }

    public String getEmail(){
        return prop.getProperty("email");
    }

    public String getPassword(){
        return prop.getProperty("password");
    }

    public String getAuthorization(){
        return prop.getProperty("authorization");
    }

    public String getAdminAuthHeader(){
        return prop.getProperty("admin.auth.header");
    }

    public String getSuperAdminAuthHeader(){
        return prop.getProperty("super.admin.auth.header");
    }


}

package ui.utils;

import com.codeborne.selenide.Selenide;

public class ClickUtil {

    public void click(String selector) {
        String script = String.format("document.querySelector('%s').click()",
                validateSelector(selector));
        Selenide.executeJavaScript(script);
    }

    private String validateSelector(String selector){
        return selector.contains("'") ?
                selector.replaceAll("'", "\"") : selector;
    }
}

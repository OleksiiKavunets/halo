package ui.pageobjects.widgets;

import com.codeborne.selenide.SelenideElement;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$;

public class ContentStructureTagWidget {

    private SelenideElement widget;

    public ContentStructureTagWidget(String selector){
        widget = $(selector);
    }

    public List<String> getAppliedTags(){
        return widget.findAll("button div[style*=\"box-sizing\"] span:nth-child(2)")
                .stream().map(s -> s.text()).collect(Collectors.toList());
    }
}

package ui.pageobjects.widgets;

import com.codeborne.selenide.SelenideElement;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$;

public class ContentStructureMultilanguageWidget {

    private SelenideElement widget;

    public ContentStructureMultilanguageWidget(String selector){
        widget = $(selector);
    }

    public List<String> getAppliedLanguages(){
        return widget.findAll("button div[style*=\"box-sizing\"] span:nth-child(2)")
                .stream().map(s -> s.text()).collect(Collectors.toList());
    }
}

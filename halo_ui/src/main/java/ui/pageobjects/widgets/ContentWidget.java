package ui.pageobjects.widgets;

import com.codeborne.selenide.SelenideElement;

import java.util.List;

import static com.codeborne.selenide.Selenide.$;

public class ContentWidget {

    private SelenideElement widget;
    private List<ContentWidget> widgets;

    private String name = "div.Grid.FlexTable__Grid div:nth-child(3) > span";
    private String itemsAmount = "div.Grid.FlexTable__Grid div:nth-child(6) > span";
    private String tagsBtn = "div:nth-child(8) > div > div > button";
    private String editBtn = "div:nth-child(9) > span button";
    private String deleteBtn = "div:nth-child(10) > span button";

    public ContentWidget(String selector){
        widget = $(selector);
    }

    public ContentWidget(SelenideElement element){
        widget = element;
    }

    public SelenideElement getContentName(){
        return widget.find(name);
    }

    public SelenideElement getContentItemsAmount() {
        return widget.find(itemsAmount);
    }

    public void clickTagsBtn(){
        widget.find(tagsBtn).click();
    }

    public void clickEditBtn(){
        widget.find(editBtn).click();
    }

    public void clickDeleteBtn(){
        widget.find(deleteBtn).click();
    }

    public List<ContentWidget> getWidgetsList(){
        return widgets;
    }
}

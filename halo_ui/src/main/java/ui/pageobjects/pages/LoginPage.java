package ui.pageobjects.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import common.pojo.User;
import ui.pageobjects.widgets.HeaderWidget;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.Wait;

public class LoginPage {


    private SelenideElement emailInput = $("input[name='email']");
    private SelenideElement passInput = $("input[name='password']");
    private SelenideElement submitBtn = $("button[type=\"submit\"]");

    public LoginPage open() {
        Selenide.open("/");
        return this;
    }

    public LoginPage setEmail(String email) {
        emailInput.val(email);
        return this;
    }

    public LoginPage setPassword(String password) {
        passInput.val(password);
        return this;
    }

    public void clickSubmitBtn() {
        submitBtn.click();
    }

    public void loginAs(User user) {
        open();
        if (!new HeaderWidget().logo.isDisplayed()) {
            setEmail(user.getEmail())
                    .setPassword(user.getPassword())
                    .clickSubmitBtn();
            Wait().withTimeout(Duration.ofSeconds(30))
                    .until(ExpectedConditions.visibilityOf(new HeaderWidget().logo));
        }
    }

}

package ui.pageobjects.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import ui.pageobjects.modals.WarningModal;
import ui.pageobjects.widgets.ContentWidget;
import ui.pageobjects.modals.CreateNewStructureModal;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ContentStructuresListPage {

    private SelenideElement createNewStructureBtn = $("button span[class=\"haloicon-create\"]");


    public CreateNewStructureModal createNewStructureModal = new CreateNewStructureModal(".EditDialog");
    public ContentWidget contentWidget = new ContentWidget(".FlexTable__row");
    public WarningModal warningModal = new WarningModal("[class=\"swal2-modal swal2-show\"]");

    public void open(){
        Selenide.open("/cms/#/modules");
    }

    public void clickCreateNewStructureBtn(){
        createNewStructureBtn.click();
    }

    public ContentWidget getContentStructureWithName(String name){
        return getContentStructures().stream().filter(s -> s.getContentName().text().equals(name)).findFirst().orElse(null);
    }

    public List<ContentWidget> getContentStructures(){
        return $$(".FlexTable__row").stream().map(s ->  new ContentWidget(s)).collect(Collectors.toList());
    }

    public boolean isContentStructurePresent(String name){
        return getContentStructureWithName(name) != null;
    }


}

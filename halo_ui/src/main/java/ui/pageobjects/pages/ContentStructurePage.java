package ui.pageobjects.pages;

import com.codeborne.selenide.SelenideElement;
import ui.pageobjects.modals.EditContentStructureModal;
import ui.pageobjects.widgets.ContentStructureMultilanguageWidget;
import ui.pageobjects.widgets.ContentStructureTagWidget;

import static com.codeborne.selenide.Selenide.$;

public class ContentStructurePage {

    SelenideElement nameInput = $("[name=\"name\"]");
    SelenideElement addNewFieldBtn = $("[title=\"Fields\"] + div button");
    SelenideElement addLanguagesBtn = $("[title=\"Multilanguage\"] + div button");
    SelenideElement addTagsBtn = $("[title=\"Content Tags\"] + div button");

    public ContentStructureMultilanguageWidget multilanguageWidget =
            new ContentStructureMultilanguageWidget("div[class=TagSelector]:nth-child(1)");

    public ContentStructureTagWidget tagWidget = new ContentStructureTagWidget("div[class=TagSelector]:nth-child(2)");

    public EditContentStructureModal modal = new EditContentStructureModal("[class=\"EditDialog\"]");

    public ContentStructurePage setName(String name){
        nameInput.clear();
        nameInput.val(name);
        return this;
    }

    public ContentStructurePage clickAddNewFieldBtn(){
        addNewFieldBtn.click();
        return this;
    }

    public ContentStructurePage clickAddLanguageBtn(){
        addLanguagesBtn.click();
        return this;
    }

    public ContentStructurePage clickAddTagBtn(){
        addTagsBtn.click();
        return this;
    }

}

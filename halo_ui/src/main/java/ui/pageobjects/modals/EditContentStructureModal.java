package ui.pageobjects.modals;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class EditContentStructureModal {
    private SelenideElement modal;
    private String modifyBtn = "[class=\"backgroundButton\"] button";
    private String element = "[class*=\"FlexTable__rowColumn\"]:nth-child(2) > span";
    private String searchField = "input[name=\"searchText\"]";

    public EditContentStructureModal(String selector){
        modal = $(selector);
    }

    private EditContentStructureModal searchElement(String elementName){
        modal.find(searchField).val(elementName);
        return this;
    }

    private EditContentStructureModal selectElement(String elementName){
        modal.findAll(element).forEach(s -> {
            if(s.text().equals(elementName)) s.click();
        });
        return this;
    }

    public EditContentStructureModal addElement(String...elementNames){
        for(String l : elementNames){
            searchElement(l).selectElement(l);
        }
        clickModifyBtn();
        return this;
    }

    private void clickModifyBtn(){
        modal.find(modifyBtn).click();
    }
}

package ui.pageobjects.modals;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class WarningModal {

    private SelenideElement modal;

    private String confirmBtn = "button[class*=\"confirm\"]";
    private String cancelBtn = "button[class*=\"cancel\"]";

    public WarningModal(String selector){
        modal = $(selector);
    }

    public void clickConfirmBtn(){
        modal.find(confirmBtn).click();
    }

    public void clickCancelBtn(){
        modal.find(cancelBtn).click();
    }

}

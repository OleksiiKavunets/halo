package ui.pageobjects.modals;

import com.codeborne.selenide.SelenideElement;
import ui.utils.ClickUtil;
import org.openqa.selenium.support.ui.ExpectedConditions;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.Wait;

public class CreateNewStructureModal {

    private SelenideElement modal;

    String nameInput = "input[id*='Contentstructurename']";
    String singleOnRadioBtn = "[type='radio'][value='on']";
    String singleOffRadioBtn = "[type='radio'][value='off']";

    public CreateNewStructureModal(String selector){
        modal = $(selector);
    }
    public CreateNewStructureModal setName(String name){
        modal.find(nameInput).val(name);
        return this;
    }

    public CreateNewStructureModal setSingle(boolean isSingle){
        if(isSingle) new ClickUtil().click(singleOnRadioBtn);
        else new ClickUtil().click(singleOffRadioBtn);
        return this;
    }

    public void clickCreateBtn(){
        modal.find("button[type='button']").click();
        Wait().until(ExpectedConditions.invisibilityOf(modal));
    }

}

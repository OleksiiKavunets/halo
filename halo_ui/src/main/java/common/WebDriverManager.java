package common;

import com.codeborne.selenide.Configuration;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import io.github.bonigarcia.wdm.OperaDriverManager;
import io.github.bonigarcia.wdm.PhantomJsDriverManager;

public class WebDriverManager {

    public static void setDriver(final String browser) {

        switch (browser) {
            case "chrome":
                ChromeDriverManager.getInstance().setup();
                Configuration.browser = "chrome";
                break;
            case "firefox":
                FirefoxDriverManager.getInstance().setup();
                Configuration.browser = "firefox";
                break;
            case "phantomjs":
                PhantomJsDriverManager.getInstance().setup();
                Configuration.browser = "phantomjs";
                break;
        }
    }
}

package common.utils;

import java.lang.reflect.Array;
import java.util.List;

public class CollectionUtil {

    private CollectionUtil(){}

    public  static <T> T[] toArray(final List<T> list){
        if(list != null && list.size() > 0) {
            final T t = list.get(0);
            T[] array = (T[]) Array.newInstance(t.getClass(), list.size());
            for (int i = 0; i < list.size(); i++) {
                array[i] = list.get(i);
            }
            return array;
        }else {
            throw new IllegalArgumentException("");
        }
    }
}

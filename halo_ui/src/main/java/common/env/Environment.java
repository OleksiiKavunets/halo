package common.env;


import common.utils.PropertyUtil;

public class Environment {

    private static Environment instance;
    private Server server;
    private PropertyUtil prop;

    private Server defaultServer = Server.STAGE;

    private Environment() {
        server = System.getProperty("server") != null ? Server.valueOf(System.getProperty("server")) : defaultServer;
        prop = new PropertyUtil(String.format("/envproperties/%s_env.property", server.toString().toLowerCase()));
    }

    public static synchronized Environment getInstance(){
        if(instance == null)
            instance = new Environment();
        return instance;
    }

    public String getBaseUrl() {
        return prop.getProperty("base.url");
    }

    public String getEmail(){
        return prop.getProperty("email");
    }

    public String getPassword(){
        return prop.getProperty("password");
    }


}

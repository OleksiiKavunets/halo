package common.env;

public enum Server {

    TEST,
    STAGE,
    PROD

}

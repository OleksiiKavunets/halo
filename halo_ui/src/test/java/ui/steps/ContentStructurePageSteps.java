package ui.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.Assert;
import ui.pageobjects.pages.ContentStructurePage;

public class ContentStructurePageSteps {

    String newName = RandomStringUtils.randomAlphabetic(10);

    ContentStructurePage page = new ContentStructurePage();

    @When("User enters content structure new name")
    public void userEntersNewName(){
        page.setName(newName);
    }

    @And("User adds language \"(.*)\"")
    public void userAddsLanguages(String language){
        page.clickAddLanguageBtn()
                .modal.addElement(language);
    }

    @And("User adds tag \"(.*)\"")
    public void userAddsTag(String tag){
        page.clickAddTagBtn().modal.addElement(tag);
    }

    @Then("User verifies language \"(.*)\" to be added")
    public void userVerifiesLanguageToBeAdded(String language){
        Assert.assertTrue(page.multilanguageWidget.getAppliedLanguages().contains(language),
                "NEW ADDED LANGUAGE WAS NOT FOUND");
    }

    @And("User verifies tag \"(.*)\" to be added")
    public void userVerifiesTagToBeAdded(String tag){
        Assert.assertTrue(page.tagWidget.getAppliedTags().contains(tag),
                "NEW ADDED TAG WAS NOT FOUND");
    }

}

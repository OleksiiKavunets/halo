package ui.steps;

import common.env.Environment;
import common.pojo.User;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import ui.pageobjects.pages.LoginPage;

public class LoginSteps  {

    LoginPage loginPage = new LoginPage();
    Environment env = Environment.getInstance();

    User user = new User(env.getEmail(), env.getPassword());

    @Given("User opens Login page")
    public void userOpensLoginPage(){
        loginPage.open();
    }

    @Given("User logins")
    public void userLogins(){
        loginPage.loginAs(user);
    }

    @When("User enters his credentials")
    public void userEntersCredentials(){
        loginPage.setEmail(env.getEmail())
                .setPassword(env.getPassword());
    }

    @And("User clicks 'Submit' button")
    public void userClicksSubmitButton(){
        loginPage.clickSubmitBtn();
    }

    @Then("User is redirected to Dashboard page")
    public void userOnDashboardPage(){
        System.out.println();
    }
}

package ui.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import ui.pageobjects.pages.ContentStructuresListPage;
import org.apache.commons.lang.RandomStringUtils;
import org.testng.Assert;

public class ContentStructuresPageListSteps {

    private String contentName;

    ContentStructuresListPage page = new ContentStructuresListPage();

    String contentRandomName = RandomStringUtils.randomAlphabetic(10);

    @When("User opens Content Structures page")
    public void userOpensContentStructurePage() {
        page.open();
    }

    @And("User creates a new content structure")
    public void userCreatesNewContentStructure() {
        System.out.println("CREATED " + contentRandomName);
        page.clickCreateNewStructureBtn();
        page.createNewStructureModal.setName(contentRandomName).setSingle(true).clickCreateBtn();
    }

    @Then("New content structure must appear on Content Structure page")
    public void newContentMustAppearOnPage() {
        System.out.println("SEEK " + contentRandomName);
        Assert.assertTrue(page.isContentStructurePresent(contentRandomName), "NEW CONTENT STRUCTURE WAS NOT FOUND ON PAGE");
    }

    @And("User opens Content Structure to edit")
    public void userOpenContentStructureToEdit(){
        page.contentWidget.clickEditBtn();
    }

    @When("User removes content structure")
    public void userRemovesContentStructure(){
        contentName = page.getContentStructures().get(0).getContentName().text();
        page.getContentStructureWithName(contentName).clickDeleteBtn();
        page.warningModal.clickConfirmBtn();
    }

    @Then("User verifies content structure to be removed")
    public void userVerifiesContentStructureToBeRemoved(){
        Assert.assertFalse(page.isContentStructurePresent(contentName),
                "CONTENT STRUCTURE WAS NOT REMOVED");
    }
}

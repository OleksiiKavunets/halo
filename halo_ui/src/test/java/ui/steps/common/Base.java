package ui.steps.common;

import com.codeborne.selenide.Configuration;
import common.WebDriverManager;
import common.env.Environment;
import cucumber.api.java.Before;

public class Base {
    @Before
    public void setUp(){
        new WebDriverManager().setDriver("chrome");
        Configuration.baseUrl = Environment.getInstance().getBaseUrl();
        Configuration.pageLoadStrategy = "normal";
    }
}

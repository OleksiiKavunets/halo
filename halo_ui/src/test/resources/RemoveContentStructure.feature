Feature: User can remove content structure

  Background:
    Given User logins

  Scenario: User removes content structure and verifies it to be removed
    Given User opens Content Structures page
    When User removes content structure
    Then User verifies content structure to be removed

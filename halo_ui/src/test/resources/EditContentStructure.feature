Feature: User can edit content structure

  Background:
    Given User logins

  Scenario: User edits content structure and verifies it to be edited
    Given User opens Content Structures page
    And User opens Content Structure to edit
    When User enters content structure new name
    And User adds language "Zulu"
    And User adds tag "Vanuatu"
    Then User verifies language "Zulu" to be added
    And User verifies tag "Vanuatu" to be added

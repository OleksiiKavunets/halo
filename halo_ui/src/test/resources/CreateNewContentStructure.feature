Feature: User can create a new content structure

  Background:
    Given User logins

  Scenario: User creates a new content structure and verifies it to be present on Content Structure page

    When User opens Content Structures page
    And User creates a new content structure
    Then New content structure must appear on Content Structure page

